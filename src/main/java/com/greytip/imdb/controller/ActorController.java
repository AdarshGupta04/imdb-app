package com.greytip.imdb.controller;

import com.greytip.imdb.model.Actor;
import com.greytip.imdb.response.ActorResponse;
import com.greytip.imdb.services.ActorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/actor")
public class ActorController {

    @Autowired
    private ActorService actorService;

    @PostMapping("")
    public ResponseEntity<ActorResponse> createActor(@RequestBody Actor actor){
        boolean result = actorService.saveActor(actor);

        if(result){
            ActorResponse actorResp = new ActorResponse("Success", "Actor successfully Registered.");
            return new ResponseEntity<ActorResponse>(actorResp, HttpStatus.CREATED);
        }

        ActorResponse actorResp = new ActorResponse("Fail", "Actor does not Registered.");
        return new ResponseEntity<ActorResponse>(actorResp, HttpStatus.BAD_REQUEST);
    }

    @GetMapping("")
    public ResponseEntity<ActorResponse> getAllActor(){
        List<Actor> actors = actorService.getAllActor();

        ActorResponse actorResp = new ActorResponse("Success", actors);
        return new ResponseEntity<ActorResponse>(actorResp, HttpStatus.ACCEPTED);
    }

    @GetMapping("/{id}")
    public  ResponseEntity<ActorResponse> getActor(@PathVariable int id){
        Actor actor =  actorService.getActor(id);

        ActorResponse actorResp = new ActorResponse("Success", actor);
        return new ResponseEntity<ActorResponse>(actorResp, HttpStatus.ACCEPTED);
    }
}
