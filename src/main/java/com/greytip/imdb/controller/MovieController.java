package com.greytip.imdb.controller;

import com.greytip.imdb.model.Movie;
import com.greytip.imdb.response.MovieResponse;
import com.greytip.imdb.services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/movie")
public class MovieController {

    @Autowired
    private MovieService movieService;

    @PostMapping("")
    public ResponseEntity<MovieResponse> createMovie(@ModelAttribute Movie movie){
        boolean result = movieService.saveMovie(movie);
        if(result){
            MovieResponse movieResp = new MovieResponse("Success", "Movie successfully Added.");
            return new ResponseEntity<MovieResponse>(movieResp, HttpStatus.CREATED);
        }

        MovieResponse movieResp = new MovieResponse("Fail", "Movie does not Added.");
        return new ResponseEntity<MovieResponse>(movieResp, HttpStatus.BAD_REQUEST);
    }

    @GetMapping("")
    public ResponseEntity<MovieResponse> getAllMovies(){
        List<Movie> movies =  movieService.getAllMovies();

        MovieResponse movieResp = new MovieResponse("Success", movies);
        return new ResponseEntity<MovieResponse>(movieResp, HttpStatus.ACCEPTED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<MovieResponse> getMovie(@PathVariable int id){
        Movie movie = movieService.getMovie(id);

        MovieResponse movieResp = new MovieResponse("Success", movie);
        return new ResponseEntity<MovieResponse>(movieResp, HttpStatus.ACCEPTED);

    }

    @PatchMapping("/{id}")
    public ResponseEntity<MovieResponse> updateMovie(@ModelAttribute Movie movie, @PathVariable int id){
        movieService.updateMovie(movie, id);
        MovieResponse movieResp = new MovieResponse("Success", "Movie successfully Added.");
        return new ResponseEntity<MovieResponse>(movieResp, HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<MovieResponse> deleteMovie(@PathVariable int id){
        movieService.deleteMovie(id);
        MovieResponse movieResp = new MovieResponse("Success", "Movie successfully Deleted.");
        return new ResponseEntity<MovieResponse>(movieResp, HttpStatus.ACCEPTED);
    }

}
