package com.greytip.imdb.controller;

import com.greytip.imdb.model.Producer;
import com.greytip.imdb.response.ProducerResponse;
import com.greytip.imdb.services.ProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/producer")
public class ProducerController {

    @Autowired
    private ProducerService producerService;

    @PostMapping("")
    public ResponseEntity<ProducerResponse> createProducer(@RequestBody Producer producer){
        boolean result = producerService.saveProducer(producer);
        if(result){
            ProducerResponse producerResp = new ProducerResponse("success", "Producer successfully registered.");
            return new ResponseEntity<ProducerResponse>(producerResp, HttpStatus.CREATED);
        }
        ProducerResponse producerResp = new ProducerResponse("Fail", "Producer can not be registered.");
        return new ResponseEntity<ProducerResponse>(producerResp, HttpStatus.BAD_REQUEST);
    }

    @GetMapping("")
    public ResponseEntity<ProducerResponse> getAllProducer(){
        List<Producer> producers = producerService.getAllProducer();

        ProducerResponse producerResp = new ProducerResponse("success", producers);
        return new ResponseEntity<ProducerResponse>(producerResp, HttpStatus.ACCEPTED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProducerResponse> getProducer(@PathVariable int id){
        Producer producer = producerService.getProducer(id);

        ProducerResponse producerResp = new ProducerResponse("Success", producer);
        return new ResponseEntity<ProducerResponse>(producerResp, HttpStatus.ACCEPTED);
    }
}
