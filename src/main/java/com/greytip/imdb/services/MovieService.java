package com.greytip.imdb.services;

import com.greytip.imdb.model.Movie;

import java.util.List;

public interface MovieService {

    boolean saveMovie(Movie movie);
    List<Movie> getAllMovies();
    Movie getMovie(int id);
    void deleteMovie(int id);
    void updateMovie(Movie movie, int id);
}
