package com.greytip.imdb.services;

import com.greytip.imdb.model.Actor;

import java.util.List;

public interface ActorService {

    boolean saveActor(Actor actor);
    List<Actor> getAllActor();
    Actor getActor(int id);

}
