package com.greytip.imdb.services.implementation;

import com.greytip.imdb.exception.ProducerNotFoundException;
import com.greytip.imdb.model.Producer;
import com.greytip.imdb.repository.ProducerRepository;
import com.greytip.imdb.services.ProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProducerServiceImpl implements ProducerService {

    @Autowired
    private ProducerRepository producerRepo;

    @Override
    public boolean saveProducer(Producer producer) {
        try{
            producerRepo.save(producer);
        }catch(DataIntegrityViolationException ex){
            throw new DataIntegrityViolationException("Already registered.");
        }catch(Exception ex){
            return false;
        }
        return true;
    }

    @Override
    public List<Producer> getAllProducer() {
        return producerRepo.findAll();
    }

    @Override
    public Producer getProducer(int id) {
        Optional<Producer> optional = producerRepo.findById(id);

        if(optional.isEmpty()){
            throw new ProducerNotFoundException();
        }

        return optional.get();
    }

}
