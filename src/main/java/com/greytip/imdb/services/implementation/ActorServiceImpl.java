package com.greytip.imdb.services.implementation;

import com.greytip.imdb.exception.ActorNotFoundException;
import com.greytip.imdb.model.Actor;
import com.greytip.imdb.repository.ActorRepository;
import com.greytip.imdb.services.ActorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ActorServiceImpl implements ActorService {

    @Autowired
    private ActorRepository actorRepo;


    @Override
    public boolean saveActor(Actor actor) {
        try{
            actorRepo.save(actor);
        }catch(DataIntegrityViolationException ex){
            throw new DataIntegrityViolationException("Actor already registered.");
        }catch(Exception ex){
            return false;
        }

        return true;
    }

    @Override
    public List<Actor> getAllActor() {
        return actorRepo.findAll();
    }

    @Override
    public Actor getActor(int id) {
        Optional<Actor> optional = actorRepo.findById(id);
        if(optional.isEmpty()){
            throw new ActorNotFoundException();
        }
        return optional.get();
    }


}
