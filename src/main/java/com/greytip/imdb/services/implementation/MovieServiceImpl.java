package com.greytip.imdb.services.implementation;

import com.greytip.imdb.exception.MovieNotCreatedException;
import com.greytip.imdb.exception.MovieNotFoundException;
import com.greytip.imdb.model.Movie;
import com.greytip.imdb.repository.MovieRepository;
import com.greytip.imdb.services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class MovieServiceImpl implements MovieService {

    @Autowired
    private MovieRepository movieRepo;

    @Autowired
    private AWSService awsService;

    @Override
    public boolean saveMovie(Movie movie) {
        try{
            String imgUrl = awsService.uploadFile(movie.getImage());
            movie.setImageUrl(imgUrl);
            movieRepo.save(movie);
        }catch(DataIntegrityViolationException ex){
            throw new DataIntegrityViolationException("Already registered.");
        }catch (Exception ex){
            throw new MovieNotCreatedException();
        }
        return true;
    }

    @Override
    public List<Movie> getAllMovies() {
        return movieRepo.findAll();
    }

    @Override
    public Movie getMovie(int id) {
        Optional<Movie> optional = movieRepo.findById(id);

        if(optional.isPresent()){
             return optional.get();
        }

        throw new MovieNotFoundException();
    }

    @Override
    public void updateMovie(Movie movie, int id) {
        Movie existingMovie = getMovie(id);

        if(existingMovie == null){
            throw new MovieNotFoundException();
        }

        String[] image = existingMovie.getImageUrl().split(".com/");
        awsService.deleteFile(image[1]);
        String imgUrl =  awsService.uploadFile(movie.getImage());

        existingMovie.setName(movie.getName());
        existingMovie.setPlot(movie.getPlot());
        existingMovie.setReleaseYear(movie.getReleaseYear());
        existingMovie.setActors(movie.getActors());
        existingMovie.setProducer(movie.getProducer());
        existingMovie.setImageUrl(imgUrl);

        movieRepo.save(existingMovie);
    }

    @Override
    public void deleteMovie(int id) {
        try{
            Movie existingMovie = getMovie(id);
            String imgName = existingMovie.getImageUrl().split(".com/")[1];
            awsService.deleteFile(imgName);

            movieRepo.deleteById(id);
        }catch(EmptyResultDataAccessException ex){
            throw new MovieNotFoundException();
        }
    }

}
