package com.greytip.imdb.services;

import com.greytip.imdb.model.Producer;

import java.util.List;

public interface ProducerService {

    boolean saveProducer(Producer producer);
    List<Producer> getAllProducer();
    Producer getProducer(int id);
}
