package com.greytip.imdb.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.greytip.imdb.model.Movie;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter @Setter  @JsonInclude(JsonInclude.Include.NON_NULL)
public class MovieResponse {
    private String status;
    private String message;
    private Movie movie;
    private List<Movie> movies;

    public MovieResponse(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public MovieResponse(String status, Movie movie) {
        this.status = status;
        this.movie = movie;
    }

    public MovieResponse(String status, List<Movie> movies) {
        this.status = status;
        this.movies = movies;
    }
}
