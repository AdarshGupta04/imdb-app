package com.greytip.imdb.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.greytip.imdb.model.Producer;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter @Setter  @JsonInclude(JsonInclude.Include.NON_NULL)
public class ProducerResponse {
    private String status;
    private String message;
    private Producer producer;
    private List<Producer> producers;

    public ProducerResponse(String status, String message){
        this.status = status;
        this.message = message;
    }

    public ProducerResponse(String status, Producer producer){
        this.status = status;
        this.producer = producer;
    }

    public ProducerResponse(String status, List<Producer> producers){
        this.status = status;
        this.producers = producers;
    }
}
