package com.greytip.imdb.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.greytip.imdb.model.Actor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
public class ActorResponse {
    private String status;
    private String message;
    private Actor actor;
    private List<Actor> actors;

    public ActorResponse(String status, String message){
        this.status = status;
        this.message = message;
    }

    public ActorResponse(String status, Actor actor){
        this.status = status;
        this.actor = actor;
    }

    public ActorResponse(String status, List<Actor> actors){
        this.status = status;
        this.actors = actors;
    }
}
