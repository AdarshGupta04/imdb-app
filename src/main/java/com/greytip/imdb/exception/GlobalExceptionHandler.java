package com.greytip.imdb.exception;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler  {

    @ExceptionHandler(MovieNotFoundException.class)
    public ResponseEntity<ErrorResponse> handleMovieNotFoundException(MovieNotFoundException movieNotFound){
        ErrorResponse errorResponse = new ErrorResponse("Fail", "Movie Not Found" );
        return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ActorNotFoundException.class)
    public ResponseEntity<ErrorResponse> handleActorNotFoundException(ActorNotFoundException actorNotFound){
        ErrorResponse errorResponse = new ErrorResponse("Fail", "Actor Not Found");
        return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ProducerNotFoundException.class)
    public ResponseEntity<ErrorResponse> handleProducerNotFoundException(ProducerNotFoundException producerNotFound){
        ErrorResponse errorResponse = new ErrorResponse("Fail", "Producer Not Found");
        return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<ErrorResponse> handleDataIntegrityViolationException(DataIntegrityViolationException dataIntegrityViolation){
        ErrorResponse errorResponse = new ErrorResponse("Fail", "Already Registered!!");
        return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(MovieNotCreatedException.class)
    public ResponseEntity<ErrorResponse> handleMovieNotCreatedException(MovieNotCreatedException movieNotCreated){
        ErrorResponse errorResponse = new ErrorResponse("Fail", "Movie can not be added, Something went wrong!!");
        return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.FORBIDDEN);
    }
}
