package com.greytip.imdb.repository;

import com.greytip.imdb.model.Producer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProducerRepository extends JpaRepository<Producer, Integer> {

}
