package com.greytip.imdb.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter @Setter
@Table(name = "movies")
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name", unique = true)
    private String name;

    private String plot;

    @Column(name = "release_year")
    private int releaseYear;

    @Column(name = "image_url")
    private String imageUrl;

    @Transient
    private MultipartFile image;

    @ManyToMany(cascade = { CascadeType.MERGE, CascadeType.REFRESH },
                fetch = FetchType.LAZY)
    @JoinTable(name = "actor_movies",
                joinColumns = {@JoinColumn(name = "movie_id"),},
                inverseJoinColumns = {@JoinColumn(name = "actor_id")})
    private List<Actor> actors;

    @OneToOne(cascade = {CascadeType.MERGE, CascadeType.REFRESH})
    private Producer producer;

}
